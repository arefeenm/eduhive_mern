import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Header from './components/Header';
import Footer from './components/Footer';

import Home from './pages/Home';
import Login from './pages/Login';


function App () {
  return (
    <React.Fragment>
      <Header/>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/registration" component={Login} />
        </Switch>
      </BrowserRouter>
      <Footer/>
    </React.Fragment>
  );
}

export default App;
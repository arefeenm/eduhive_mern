const bodyParser = require('body-parser'),
  express = require('express'),
  fs = require('fs'),
  path = require('path');

const router = express.Router();
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

router.get('/_status', (req, res) => {
  res.send('Status OK!');
});

app.use(router);

app.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send('Missing authentication credentials.');
  }
});

app.listen(8081, err => {
  if (err) { process.exit(1); }
  console.log('Server is up and running on port numner 8081.');

  // require('./utils/db');

  fs.readdirSync(path.join(__dirname, 'routes')).map(file => {
    require('./routes/' + file)(app);
  });
});

module.exports = app;